﻿// Alan Zucconi
// www.alanzucconi.com
using UnityEngine;
using System.Collections;
using Tobii.Gaming;

public class Heatmap : MonoBehaviour
{
    [HideInInspector]
    public static Vector2[] heatpoints;

    public Material heatmapMaterial;
    public float radius;
    [Range(0, 1)]
    public float intensity;

    private int nPos;
    private Vector4[] points;
    private float[] radiuses;
    private float[] intensities;


    private Vector4[] properties;
    private bool even;
    Camera camera;


    //gaze members
    private int heatmapHistory = 50;

    //gaze point cloud (heatmap color mapping)
    private const float MaxVisibleDurationInSeconds = 0.5f;
    private GazePoint[] gazebuffer;
    private ArrayList gazepoints;
    private int gazeCounter = 0;
    public static int _last;
    private int _counter;
    private int lastCounter;

    void OnEnable()
    {
        startup();
    }

    void Start()
    {
        startup();
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    private void startup()
    {
        even = (heatpoints.Length % 2 == 0);
        nPos = heatmapHistory;
        InitializeGazeBuffer();
        points = new Vector4[nPos];
        radiuses = new float[nPos];
        intensities = new float[nPos];
        properties = new Vector4[nPos];
        heatmapMaterial.SetInt("_Points_Length", points.Length);
        InitializeProperties();

        _last = 0;
        _counter = heatmapHistory - 1;
    }

    void Update()
    {
        if(heatpoints.Length != 0 && _last <= heatpoints.Length)
        {
            
            updateProperties();
            updateLocations();
        }
        
        /*
            GazePoint gazePoint = TobiiAPI.GetGazePoint();

            if (gazePoint.IsRecent() && gazePoint.Timestamp > (_lastGazePoint.Timestamp + float.Epsilon) && gazePoint.IsValid)
            {
                UpdateGazePointCloud(gazePoint);
                updateProperties();
                updateLocations();
            }
        */
    }

    private void InitializeProperties()
    {
        for (int i = 0; i < points.Length; i++)
        {
            radiuses[i] = 0.0f;
            intensities[i] = 0.0f;
            properties[i] = new Vector2(radiuses[i], intensities[i]);
        }
        heatmapMaterial.SetVectorArray("_Properties", properties);
    }

    private int NextSave()
    {
        return _last++;
    }

    private int NextHeatmap()
    {
        return ((_counter + 1) % heatmapHistory);
    }

    private void InitializeGazeBuffer()
    {
        gazebuffer = new GazePoint[heatmapHistory];
        for (int i = 0; i < heatmapHistory; i++)
            gazebuffer[i] = GazePoint.Invalid;
        gazepoints = new ArrayList();
    }

    private void UpdateGazePointCloud(GazePoint point)
    {
        gazepoints.Add(point);
    }

    private void updateProperties()
    {
        float r = radius * transform.root.localScale.x;
        lastCounter = _counter;
        
        for(int i = 0; i < 2; i++)
        {
            radiuses[_counter] = r;
            intensities[_counter] = intensity;
            properties[_counter] = new Vector2(radiuses[_counter], intensities[_counter]);
            _counter = NextHeatmap();
        }

        //for heatmap in videopanel
        /*
        radiuses[_counter] = r;
        intensities[_counter] = intensity;
        properties[_counter] = new Vector2(radiuses[_counter], intensities[_counter]);
        //_counter = NextHeatmap();*/
        
        heatmapMaterial.SetVectorArray("_Properties", properties);
    }

    private void updateLocations()
    {
        _counter = lastCounter;
        Vector2[] v = new Vector2[2];

        for(int i = 0; i < 2; i++)
        {
            //Debug.Log(_last + "/nLength = " + heatpoints.Length);
            if(!even)
                if(_last >= heatpoints.Length)
                {
                    //Debug.Log("got here");
                    break;
                }
            if(_last >= heatpoints.Length)
            {
                //Debug.Log("got herex2");
                break;
            }
            v[i] = heatpoints[NextSave()];
        }
        
        _counter = lastCounter;
        
        for(int i = 0; i < 2; i++)
        {
            if(!even)
            {
                if(_last == heatpoints.Length)
                {
                    Vector3 p = camera.ViewportToWorldPoint(
                        new Vector3(v[0].x, v[0].y, -camera.transform.localPosition.z + 1));
                    points[_counter] = p;
                    break;
                }
                else
                {
                    Vector3 p = camera.ViewportToWorldPoint(
                        new Vector3(v[i].x, v[i].y, -camera.transform.localPosition.z + 1));
                    points[_counter] = p;
                }
            }
            else
            {
                Vector3 p = camera.ViewportToWorldPoint(
                    new Vector3(v[i].x, v[i].y, -camera.transform.localPosition.z + 1));
                points[_counter] = p;
            }
        }
        
        //for heatmap in videopanel
        /*
        _counter = lastCounter;
        //_last = NextSave();
        Debug.Log(NextSave());
        gazebuffer[_counter] = (GazePoint) gazepoints[_last];

        Vector3 p = camera.ViewportToWorldPoint(new Vector3(gazebuffer[_counter].Viewport.x,
                                gazebuffer[_counter].Viewport.y, -camera.transform.localPosition.z + 1));
        points[_counter] = p;
        */

        _counter = NextHeatmap();
        heatmapMaterial.SetVectorArray("_Points", points);
    }


}