﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Algorithms : MonoBehaviour {

    private static uint[] lookup32;
    private static char[] result;
    private static uint[] theResult;
    private static uint val;
    private static string s;
    private static int aux;
    private static readonly uint[] _lookup32 = CreateLookup32();
    private static List<byte> bytes;


    public static void LeftShift<T>(T[] arr, int n)
    {
        Array.Copy(arr, n, arr, 0, arr.Length - n);
    }

    public static int Bytes2Int(byte b1, byte b2, byte b3)
    {
        int r = 0;
        byte b0 = 0xff;

        if ((b1 & 0x80) != 0) r |= b0 << 24;
        r |= b1 << 16;
        r |= b2 << 8;
        r |= b3;
        return r;
    }

    private static uint[] CreateLookup32()
    {
        theResult = new uint[256];
        for (int i = 0; i < 256; i++)
        {
            s = i.ToString("X2");
            theResult[i] = (s[0]) + ((uint)s[1] << 16);
        }
        return theResult;
    }
    public static string ByteArrayToHexViaLookup32(byte[] bytes)
    {
        lookup32 = _lookup32;
        result = new char[bytes.Length * 2];
        for (int i = 0; i < bytes.Length; i++)
        {
            val = lookup32[bytes[i]];
            result[2 * i] = (char)val;
            result[2 * i + 1] = (char)(val >> 16);
        }
        return new string(result);
    }

    public static byte[] ToBCD(DateTime d)
    {
        bytes = new List<byte>();
        aux = (int)d.DayOfWeek;
        aux = (aux == 0) ? 7 : aux; //Sunday is seven in stm. STM32F4xx_hal_rtc.h line:305 => #define RTC_WEEKDAY_SUNDAY ((uint8_t)0x07)
        s = d.ToString("HHmmss");
        s += aux.ToString("D2");
        s += d.ToString("MMddyy");
        bytes.Add(0xAA);
        for (int i = 0; i < s.Length; i += 2)
        {
            bytes.Add((byte)((s[i] - '0') << 4 | (s[i + 1] - '0')));
        }
        return bytes.ToArray();
    }

}
