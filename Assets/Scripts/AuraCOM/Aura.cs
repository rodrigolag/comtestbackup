﻿using UnityEngine;
using System.Threading;
using System.Collections;
using System.IO.Ports;
using System;
using CielaSpike;
using System.Collections.Generic;

public class Aura : MonoBehaviour
{
    public Transform Contacts;
    public GameObject FFT_GraphsManager;

    [HideInInspector]
    public float inputFrequency = 250;

    [HideInInspector]
    public float EEG_verticalScale = 1; //1 means autoscale
    public float FFT_verticalScale { get; set; }


    [HideInInspector]
    public int seconds = 5;

    public int currentFilt_ind { get; set; }
    public int currentNotch_ind { get; set; }  // set to 0 to default to 60Hz, set to 1 to default to 50Hz

    public bool streaming { get; set; }
    public bool running { get; set; }

    private const int count = 8;
    [HideInInspector]
    public bool eventOn = false;
    [HideInInspector]
    public int event1 = 0;
    [HideInInspector]
    public object eventCounterKey = new object();
    [HideInInspector]
    public byte[] buffer = new byte[223];
    [HideInInspector]
    public float[][] dataBuffers = new float[count][];
    [HideInInspector]
    public float[][] filteredSamples = new float[count][];
    [HideInInspector]
    public List<double>[] matlabBuffers = new List<double>[count];
    [HideInInspector]
    public float[,] dataBuffersLSL = new float[9, count]; //chunks of 9 samples. 8 channels
    [HideInInspector]
    public float[][] samples = new float[count][];
    [HideInInspector]
    public float[][] samplesAux = new float[count][];
    [HideInInspector]
    public string[][] records = new string[9][];
    [HideInInspector]
    public object busyWritingMatlabFile = new object();

    private qualityManager[] qm = new qualityManager[9];
    private DrawFFT[] fft;
    private Task task;
    private Thread thread;
    private SerialPort stream;
    private string _port;
    private int _baudRate;
    private DateTime now;

    private object key = new object();

    private bool success = false;
    private bool failure = false;
    private bool falsePositive = false;
    private bool timeFlag = false;
    private float elapsedTime = 0;

    private byte[] header = { 0x01, 0x01, 0x01, 0x01 };
    private byte[] header_hw = { 0xC0, 0x00, 0x02 }; //b'\xC0\x00\x02' The header frame of the Aura board.
    private int[] statusBytes = new int[3];
    private int raw;
    private float[] MPU = new float[7];
    private const string event0 = "0";
    private float auxF;


    private static int N_FILT_CONFIGS = 5;
    private FilterConstants[] filtCoeff_bp = new FilterConstants[N_FILT_CONFIGS];
    private static int N_NOTCH_CONFIGS = 3;
    private FilterConstants[] filtCoeff_notch = new FilterConstants[N_NOTCH_CONFIGS];

    private double dataOut;
    private int Nback;
    private double[] prev_y;
    private double[] prev_x;
    private class FilterConstants
    {
        public double[] a;
        public double[] b;
        public string name;
        public string short_name;
        public FilterConstants(double[] b_given, double[] a_given, String name_given, String short_name_given)
        {
            b = new double[b_given.Length]; a = new double[b_given.Length];
            for (int i = 0; i < b.Length; i++) { b[i] = b_given[i]; }
            for (int i = 0; i < a.Length; i++) { a[i] = a_given[i]; }
            name = name_given;
            short_name = short_name_given;
        }
    }

    void Awake()
    {
        for(int i = 0; i < 8; i++)
        {
            qm[i] = GameObject.FindGameObjectWithTag("qm"+(i+1).ToString()).GetComponent<qualityManager>();
        }
        qm[8] = GameObject.FindGameObjectWithTag("srb").GetComponent<qualityManager>();
        fft = FFT_GraphsManager.GetComponentsInChildren<DrawFFT>();
        streaming = false;
        running = false;
    }

    void Start()
    {
        //Initialize parameters
        defineFilters();
        for (int i = 0; i < count; i++)
        {
            matlabBuffers[i] = new List<double>();
            dataBuffers[i] = new float[9];
            samples[i] = new float[1750];
            samplesAux[i] = new float[1750];
            filteredSamples[i] = new float[1750];
        }
        for (int i = 0; i < 9; i++)
        {
            records[i] = new string[16];
        }
    }

    public void OpenAura(string port, int baudRate)
    {
        _port = "\\\\.\\" + port;
        _baudRate = baudRate;
        resetFlags();
        if (stream != null && stream.IsOpen) // If old stream is open, close it
        {   stream.Close(); }

        stream = new SerialPort(_port, _baudRate);
        if (stream.IsOpen) //If new stream is already open, warn the user and then exit
        {
            Debug.Log("COM port is busy");
            return;
        }

        // Create the external thread
        thread = new Thread(OpenConn);
        //Start the external thread to open Aura
        thread.Start();
        //See in the Unity main thread if it was a successful opening
        StartCoroutine(checkSuccess());
    }

    private void OpenConn()
    {
        try
        {
            stream.Open();
        }
        catch (System.IO.IOException e)
        {
            lock (key) { failure = true; }
            Debug.Log(e);
            return;
        }

        elapsedTime = 0;
        lock (key) { timeFlag = true; }
        for (int i = 0; i < 5; i++)
        {
            Write(new byte[8] { 0xBE, 0xBE, 0xBE, 0xBE, 0xBE, 0xBE, 0xBE, 0xBE });
            if (stream.ReadByte() == 0xEF)
            {
                Write(Algorithms.ToBCD(DateTime.Now));
                lock (key) { success = true; }
                return;
            }
        }
        lock (key) { falsePositive = true; }
    }

    IEnumerator checkSuccess()
    {
        while (true)
        {
            if (Success())
            {
                Debug.Log("Connected");
                resetFlags();
                this.StartCoroutineAsync(serialAsync(), out task);
                break;
            }
            if (Fail())
            {
                Debug.Log("Port is already open");
                resetFlags();
                break;
            }
            if (notAura())
            {
                Debug.Log("This is not an Aura Device");
                resetFlags();
                break;
            }
            if (handshakeTimer())
            {
                if (elapsedTime > 10)
                {
                    Debug.Log("COM is stuck. Timeout.");
                    resetFlags();
                    thread.Abort();
                    break;
                }
                elapsedTime += Time.deltaTime;
            }
            yield return null;
        }
    }

    private bool Success()
    {
        lock (key)
        {
            return success;
        }
    }
    private bool Fail()
    {
        lock (key)
        {
            return failure;
        }
    }
    private bool notAura()
    {
        lock (key)
        {
            return falsePositive;
        }
    }
    private bool handshakeTimer()
    {
        lock (key)
        {
            return timeFlag;
        }
    }
    private bool doneWritingMatFile()
    {
        lock (busyWritingMatlabFile)
        {
            return CSV_recorder.doneWritingMatlabFile;
        }
    }

    IEnumerator serialAsync()
    {
        int auxIndex = 0;
        //Free the hardware buffer
        stream.BaseStream.BeginRead(buffer, 0, buffer.Length, delegate (IAsyncResult ar)
        {
            try
            {
                stream.BaseStream.EndRead(ar);
            }
            catch (TimeoutException)
            {

            }

        }, null);

        while (stream.IsOpen)
        {
            readAndRemoveHeader();
            //Try to read the complete transmission of 223 bytes
            try
            {
                for (int i = 0; i < 223; i++)
                {
                    buffer[i] = (byte)stream.ReadByte();
                }
            }
            catch (Exception e) { Debug.Log(e); break; }

            //Put the data in the corresponding channel buffers
            for (int i = 0; i < 9; i++)
            {
                lock (busyWritingMatlabFile)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        auxIndex = (j * 3) + (i * 24);
                        raw = Algorithms.Bytes2Int(buffer[auxIndex], buffer[auxIndex + 1], buffer[auxIndex + 2]); //Algorithm to convert 24_Int to 32_Int
                        dataBuffers[j][i] = (raw * .0447034889f); //Datasheet multiplication          //12x 0.0447034889f    //24x  0.0268221f
                        dataBuffersLSL[i, j] = dataBuffers[j][i]; // Buffer for LSL output in chunks of 9 samples and 8 channels

                        if (CSV_recorder.canAddMore)
                        {
                            matlabBuffers[j].Add(raw * .0447034889);
                        }

                        records[i][j] = dataBuffers[j][i].ToString(); //Strings for the CSV record file
                    }
                }

            }

            //Filter each channel data
            for (int i = 0; i < 8; i++)
            {
                Algorithms.LeftShift(samples[i], 9);
                Array.Copy(dataBuffers[i], 0, samples[i], samples[i].Length - 9, 9);
                samplesAux[i] = (float[])samples[i].Clone();
                filterIIR(filtCoeff_notch[currentNotch_ind].b, filtCoeff_notch[currentNotch_ind].a, samplesAux[i]);
                filterIIR(filtCoeff_bp[currentFilt_ind].b, filtCoeff_bp[currentFilt_ind].a, samplesAux[i]); 
            }

            MPU[0] = buffer[buffer.Length - 7];
            MPU[1] = buffer[buffer.Length - 6];
            MPU[2] = buffer[buffer.Length - 5];
            MPU[3] = buffer[buffer.Length - 4];
            MPU[4] = buffer[buffer.Length - 3];
            MPU[5] = buffer[buffer.Length - 2];
            MPU[6] = buffer[buffer.Length - 1];

            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    auxF = (MPU[j] - 128) / 64; //AccXYZ Formula. Range is +- 2G
                    records[i][8 + j] = auxF.ToString("0.00");
                }
                for (int j = 3; j < 6; j++)
                {
                    auxF = (MPU[j] - 128) / 0.512f; //Gyroscope Formula. Range is +- 250 degrees/s 
                    records[i][8 + j] = auxF.ToString("N0");
                }
                records[i][14] = MPU[6].ToString(); //Battery
                lock (eventCounterKey)
                {
                    records[i][15] = eventOn ? event1.ToString() : event0;
                }

            }

            for (int i = 0; i < 8; i++)
            {
                filteredSamples[i] = (float[])samplesAux[i].Clone();
                fft[i].updateFilteredBins(filteredSamples[i]); //Function to update the FFT script with the filtered values, otherwise, it would be drawing raw values
            }


            //Jump to MAIN UNITY THREAD //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            yield return Ninja.JumpToUnity;
            if (!eventOn) //If it was turned off, increment counter
            {
                eventOn = (Input.GetKey(KeyCode.Space) ? true : false);
                if (eventOn) { event1 += 1; }
            }
            else
            {
                eventOn = (Input.GetKey(KeyCode.Space) ? true : false);
            }

            qm[0].changeColor((statusBytes[1] & (0x1 << 4)) != 0);
            qm[1].changeColor((statusBytes[1] & (0x1 << 5)) != 0);
            qm[2].changeColor((statusBytes[1] & (0x1 << 6)) != 0);
            qm[3].changeColor((statusBytes[1] & (0x1 << 7)) != 0);

            qm[4].changeColor((statusBytes[0] & (0x1 << 0)) != 0);
            qm[5].changeColor((statusBytes[0] & (0x1 << 1)) != 0);
            qm[6].changeColor((statusBytes[0] & (0x1 << 2)) != 0);
            qm[7].changeColor((statusBytes[0] & (0x1 << 3)) != 0);

            qm[8].changeColor((statusBytes[1] & (0x1 << 0)) != 0); //REF SRB

            CSV_recorder.EEGrefresh = true;
            if (doneWritingMatFile())
            {
                for (int i = 0; i < 8; i++)
                {
                    matlabBuffers[i].Clear();
                }
            }

            // for (int i = 0; i < 8; i++)
            // {
            //     filteredSamples[i] = (float[])samplesAux[i].Clone();
            //     fft[i].updateFilteredBins(filteredSamples[i]); //Function to update the FFT script with the filtered values, otherwise, it would be drawing raw values
            // }
            streaming = true;
            running = true;
            //Exit main Unity thread and JUMP BACK TO EXTERNAL THREAD //////////////////////////////////////////////////////////////////////////////////////////////////////////
            yield return Ninja.JumpBack;


        }// <- Loop ending of while (stream.IsOpen)

        yield return Ninja.JumpToUnity;
        if (stream.IsOpen)
        {
            Debug.Log("Closing");
            stream.Close();
        }
        for (int i = 0; i < 8; i++) //Clear array
        { Array.Clear(samples[i], 0, samples[i].Length); }
        streaming = false;
        running = false;
        yield return Ninja.JumpBack;
    }

    public void Write(byte[] bytes)
    {
        stream.Write(bytes, 0, bytes.Length);
        stream.BaseStream.Flush();
    }

    private void readAndRemoveHeader()
    {
        while (stream.IsOpen)
        {
            try
            {
                if (header[0] == stream.ReadByte())
                {
                    if (header[1] == stream.ReadByte())
                    {
                        statusBytes[0] = stream.ReadByte();
                        if ((statusBytes[0] & header_hw[0]) == header_hw[0])
                        {
                            statusBytes[1] = stream.ReadByte();
                            if ((statusBytes[1] & header_hw[1]) == header_hw[1])
                            {
                                statusBytes[2] = stream.ReadByte();
                                if ((statusBytes[2] & header_hw[2]) == header_hw[2])
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e) { Debug.Log(e); break; }
        }
    }


    private void defineFilters()
    {
        int n_filt;
        double[] b, a, b2, a2;
        string filt_txt;
        string short_txt;

        //loop over all of the pre-defined filter types
        n_filt = filtCoeff_notch.Length;
        for (int Ifilt = 0; Ifilt < n_filt; Ifilt++)
        {
            switch (Ifilt)
            {
                case 0:
                    //60 Hz notch filter, assumed fs = 250 Hz.  2nd Order Butterworth: b, a = signal.butter(2,[59.0 61.0]/(fs_Hz / 2.0), 'bandstop')
                    b2 = new double[] { 9.650809863447347e-001, -2.424683201757643e-001, 1.945391494128786e+000, -2.424683201757643e-001, 9.650809863447347e-001 };
                    a2 = new double[] { 1.000000000000000e+000, -2.467782611297853e-001, 1.944171784691352e+000, -2.381583792217435e-001, 9.313816821269039e-001 };
                    filtCoeff_notch[Ifilt] = new FilterConstants(b2, a2, "Notch 60Hz", "60Hz");
                    break;
                case 1:
                    //50 Hz notch filter, assumed fs = 250 Hz.  2nd Order Butterworth: b, a = signal.butter(2,[49.0 51.0]/(fs_Hz / 2.0), 'bandstop')
                    b2 = new double[] { 0.96508099, -1.19328255, 2.29902305, -1.19328255, 0.96508099 };
                    a2 = new double[] { 1.0, -1.21449348, 2.29780334, -1.17207163, 0.93138168 };
                    filtCoeff_notch[Ifilt] = new FilterConstants(b2, a2, "Notch 50Hz", "50Hz");
                    break;
                case 2:
                    //no notch filter
                    b2 = new double[] { 1.0 };
                    a2 = new double[] { 1.0 };
                    filtCoeff_notch[Ifilt] = new FilterConstants(b2, a2, "No Notch", "None");
                    break;
            }
        } // end loop over notch filters

        n_filt = filtCoeff_bp.Length;
        for (int Ifilt = 0; Ifilt < n_filt; Ifilt++)
        {
            //define bandpass filter
            switch (Ifilt)
            {
                case 0:
                    //butter(2,[1 50]/(250/2));  %bandpass filter
                    b = new double[] { 2.001387256580675e-001, 0.0f, -4.002774513161350e-001, 0.0f, 2.001387256580675e-001 };
                    a = new double[] { 1.0f, -2.355934631131582e+000, 1.941257088655214e+000, -7.847063755334187e-001, 1.999076052968340e-001 };
                    filt_txt = "Bandpass 1-50Hz";
                    short_txt = "1-50 Hz";
                    break;
                case 1:
                    //butter(2,[7 13]/(250/2));
                    b = new double[] {
                        5.129268366104263e-003, 0.0f, -1.025853673220853e-002, 0.0f, 5.129268366104263e-003
                      };
                    a = new double[] {
                        1.0f, -3.678895469764040e+000, 5.179700413522124e+000, -3.305801890016702e+000, 8.079495914209149e-001
                      };
                    filt_txt = "Bandpass 7-13Hz";
                    short_txt = "7-13 Hz";
                    break;
                case 2:
                    //[b,a]=butter(2,[15 50]/(250/2)); %matlab command
                    b = new double[] {
                        1.173510367246093e-001, 0.0f, -2.347020734492186e-001, 0.0f, 1.173510367246093e-001
                      };
                    a = new double[] {
                        1.0f, -2.137430180172061e+000, 2.038578008108517e+000, -1.070144399200925e+000, 2.946365275879138e-001
                      };
                    filt_txt = "Bandpass 15-50Hz";
                    short_txt = "15-50 Hz";
                    break;
                case 3:
                    //[b,a]=butter(2,[5 50]/(250/2)); %matlab command
                    b = new double[] {
                        1.750876436721012e-001, 0.0f, -3.501752873442023e-001, 0.0f, 1.750876436721012e-001
                      };
                    a = new double[] {
                        1.0f, -2.299055356038497e+000, 1.967497759984450e+000, -8.748055564494800e-001, 2.196539839136946e-001
                      };
                    filt_txt = "Bandpass 5-50Hz";
                    short_txt = "5-50 Hz";
                    break;
                default:
                    //no filtering
                    b = new double[] {
                        1.0
                      };
                    a = new double[] {
                        1.0
                      };
                    filt_txt = "No BP Filter";
                    short_txt = "No Filter";
                    break;
            }  //end switch block  

            //create the bandpass filter    
            filtCoeff_bp[Ifilt] = new FilterConstants(b, a, filt_txt, short_txt);
        } //end loop over band pass filters
    } //end defineFilters method
    private void filterIIR(double[] filt_b, double[] filt_a, float[] data)
    {
        Nback = filt_b.Length;
        prev_y = new double[Nback];
        prev_x = new double[Nback];

        //step through data points
        for (int i = 0; i < data.Length; i++)
        {
            //shift the previous outputs
            for (int j = Nback - 1; j > 0; j--)
            {
                prev_y[j] = prev_y[j - 1];
                prev_x[j] = prev_x[j - 1];
            }

            //add in the new point
            prev_x[0] = data[i];

            //compute the new data point
            dataOut = 0;
            for (int j = 0; j < Nback; j++)
            {
                dataOut += filt_b[j] * prev_x[j];
                if (j > 0)
                {
                    dataOut -= filt_a[j] * prev_y[j];
                }
            }

            //save output value
            prev_y[0] = dataOut;
            data[i] = (float)dataOut;
        }
        //return data;
    }


    public void setNotch(int mode)
    {
        currentNotch_ind = mode;
    }
    public void setBandPass(int mode)
    {
        currentFilt_ind = mode;
    }
    public void set_EEG_Scale(int mode)
    {
        switch (mode)
        {
            case 0:
                EEG_verticalScale = 1;
                break;
            case 1:
                EEG_verticalScale = 50;
                break;
            case 2:
                EEG_verticalScale = 100;
                break;
            case 3:
                EEG_verticalScale = 200;
                break;
            case 4:
                EEG_verticalScale = 1000;
                break;
            case 5:
                EEG_verticalScale = 10000;
                break;
        }
    }

    public void Disconnect()
    {
        Write(new byte[8] { 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33 });
        resetFlags();
        stream.Close();
    }

    private void resetFlags()
    {
        success = false;
        failure = false;
        falsePositive = false;
        timeFlag = false;
    }

    void OnDestroy()
    {
        if (stream != null && stream.IsOpen)
        {
            Write(new byte[8] { 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33 });
            resetFlags();
            stream.Close();
        }
    }
}