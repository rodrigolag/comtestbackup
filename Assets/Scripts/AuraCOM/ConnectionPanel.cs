﻿using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;
using System.Linq;
using UnityEngine.EventSystems;

public class ConnectionPanel : MonoBehaviour
{
    public Aura Aura;
    public Sprite selectedColor;
    public Sprite normalColor;
    public Object COM;
    public Transform Theparent;

    private RectTransform rt;
    private GameObject Panel;
    private string comString;
    private string[] ports;
    private GameObject aux;

    private Vector3 open = new Vector3(1, 1, 1);
    private Vector3 closed = new Vector3(0, 0, 0);


    void Awake()
    {
        Panel = gameObject;
        rt = GetComponent<RectTransform>();
        rt.localScale = closed;
    }

    private void StartCOM(string com)
    {
        foreach (Transform child in Theparent)
        {
            if (child.transform.GetChild(0).GetComponent<Text>().text == com)
            {
                comString = com;
                child.GetComponent<Image>().sprite = selectedColor;
            }
            else
            {
                child.GetComponent<Image>().sprite = normalColor;
            }
        }
    }

    public void launchSerial()
    {

        if (comString != null)
        {
            Debug.Log(comString);
            Aura.OpenAura(comString, 230400);
        }

        EventSystem.current.SetSelectedGameObject(null); // To eliminate focus of Connect Button. This way, spacebar doesnt activate a new connection.

    }

    public void activateButtons()
    {
        foreach (Transform child in Theparent)
        {
            child.GetComponent<Button>().interactable = true;
        }
    }

    public void deactivateButtons()
    {
        foreach (Transform child in Theparent)
        {
            child.GetComponent<Button>().interactable = false;
        }
    }

    public void activateThis()
    {
        rt.localScale = open;
        comString = null;
        ports = SerialPort.GetPortNames().Distinct().ToArray();
        foreach (Transform child in Theparent)
        {
            Destroy(child.gameObject);
        }
        foreach (string port in ports)
        {
            string auxS = port;
            aux = Instantiate(COM, Theparent) as GameObject;
            aux.SetActive(true);
            aux.transform.GetChild(0).GetComponent<Text>().text = port;
            aux.GetComponent<Button>().onClick.AddListener(delegate { StartCOM(auxS); });
        }
    }

    public void deActivateThis()
    {
        comString = null;
        rt.localScale = closed;
    }

}
