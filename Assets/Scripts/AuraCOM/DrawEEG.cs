﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine.UI;

public class DrawEEG : MonoBehaviour
{
    #region Public Variables

    [Range(0, 13)]
    public int Sampler = 3;

    #endregion

    #region Private Variables
    private Aura Aura;
    private int pointsPerGraph;
    private LineRenderer lR;
    private RectTransform rt;
    private DrawFFT[] fft;
    private Vector3[] positions = new Vector3[1750];
    private Text text;
    private IEnumerable<float> auxFloat;
    private float scaling = 0.01f;
    private float max, min, auxF;
    private float sample;
    private float[] samples = new float[1750];
    private float[] scalingAux = new float[1750];
    private float[] filteredSamples;
    private float slidingDistance;
    private float oldWidth;
    private bool graphsAreEmpty = true;

    private static int N_FILT_CONFIGS = 5;
    private FilterConstants[] filtCoeff_bp = new FilterConstants[N_FILT_CONFIGS];
    private static int N_NOTCH_CONFIGS = 3;
    private FilterConstants[] filtCoeff_notch = new FilterConstants[N_NOTCH_CONFIGS];

    private double dataOut;
    private int Nback;
    private double[] prev_y;
    private double[] prev_x;

    public class FilterConstants
    {
        public double[] a;
        public double[] b;
        public string name;
        public string short_name;
        public FilterConstants(double[] b_given, double[] a_given, String name_given, String short_name_given)
        {
            b = new double[b_given.Length]; a = new double[b_given.Length];
            for (int i = 0; i < b.Length; i++) { b[i] = b_given[i]; }
            for (int i = 0; i < a.Length; i++) { a[i] = a_given[i]; }
            name = name_given;
            short_name = short_name_given;
        }
    }

    #endregion

    void Start()
    {
        rt = GetComponent<RectTransform>();
        fft = GameObject.FindGameObjectWithTag("fftManager").GetComponentsInChildren<DrawFFT>();
        lR = GetComponent<LineRenderer>();
        Aura = GameObject.FindGameObjectWithTag("graphsManager").GetComponent<EEG_GraphsManager>().Aura;
        text = GetComponent<Text>();
        //lR.enabled = true;

        oldWidth = rt.rect.width;
        pointsPerGraph = Aura.seconds * (int)Aura.inputFrequency;
        slidingDistance = 1f / pointsPerGraph;
        positions = new Vector3[pointsPerGraph];
        lR.positionCount = (pointsPerGraph);
        lR.SetPositions(positions);
        scalingAux = new float[pointsPerGraph];
        for (var i = 0; i < positions.Length; i++)
        {
            positions[i].x = ((slidingDistance) * (i + 1)) * rt.rect.width - rt.rect.width / 2;
        }
    }

    void Update()
    {
        if (Aura.streaming && Aura.running)
        {
            graphsAreEmpty = false; // Just ignore. It is to reset flag
            autoScaling();
            updatePositions();
            draw();

            auxFloat = Aura.filteredSamples[Sampler].Skip(Aura.filteredSamples[Sampler].Length - (int)Aura.inputFrequency);
            auxF = 0;
            foreach (float f in auxFloat)
            {
                auxF += Mathf.Pow(f, 2);
            }
            auxF = auxF / Aura.inputFrequency;
            auxF = Mathf.Sqrt(auxF);
            text.text = auxF.ToString("F0") + " uV";

        }
        if (!Aura.streaming)
        {
            if (!graphsAreEmpty)
            {
                for (var i = 0; i < positions.Length; i++)
                {
                    positions[positions.Length - 1 - i].y = 0 * rt.rect.height - rt.rect.height / 2;
                }
                lR.SetPositions(positions);
                graphsAreEmpty = true;
            }

            if (oldWidth != rt.rect.width)
            {
                for (var i = 0; i < positions.Length; i++)
                {
                    positions[positions.Length - 1 - i].y = 0 * rt.rect.height - rt.rect.height / 2;
                }
                lR.SetPositions(positions);
            }
            oldWidth = rt.rect.width;
        }
    }

    #region Private Methods

    private void autoScaling()
    {
        scalingAux = Aura.filteredSamples[Sampler].Skip(Aura.filteredSamples[Sampler].Length - pointsPerGraph).ToArray();
        if (Aura.EEG_verticalScale == 1)
        {
            max = scalingAux.Max();
            min = Math.Abs(scalingAux.Min());
            scaling = (max > min) ? max : min;
        }
        else
        {
            scaling = Aura.EEG_verticalScale;
        }

    }

    private void updatePositions()
    {
        for (var i = 0; i < positions.Length; i++)
        {
            positions[positions.Length - 1 - i].y = ((Aura.filteredSamples[Sampler][Aura.filteredSamples[Sampler].Length - 1 - i] / (scaling * 2)) + 0.5f) * rt.rect.height - rt.rect.height / 2;
        }

        //If resolution changes
        if (oldWidth != rt.rect.width)
        {
            for (var i = 0; i < positions.Length; i++)
            {
                positions[i].x = ((slidingDistance) * (i + 1)) * rt.rect.width - rt.rect.width / 2;
            }
        }
        oldWidth = rt.rect.width;

    }


    private void draw()
    {
        lR.SetPositions(positions);
    }

    #endregion

}