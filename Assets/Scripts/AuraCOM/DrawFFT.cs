﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Complex = AForge.Math.Complex;
using FourierTransform = AForge.Math.FourierTransform;
using Direction = AForge.Math.FourierTransform.Direction;
using UnityEngine.UI;
using System;

public class DrawFFT : MonoBehaviour
{
    #region Public Variables

    [Range(0, 7)]
    public int Sampler = 3;

    [HideInInspector]
    public plotScale mode;
    public enum plotScale { Linear, Log }

    #endregion

    #region Private Variables
    private Aura Aura;
    private FFT_GraphsManager manager;

    private LineRenderer lR;
    private RectTransform rt;

    private Queue<Complex> bins = new Queue<Complex>();
    private Complex bin;
    private Complex[] binsArray;
    private IEnumerable<double> magnitudeArray;
    private IEnumerable<Vector2> FTTpos;
    private float[] filteredValues;
    private bool canCalculateNow;

    private static float N; //2^n
    private static float Fs; //Hz
    private float binIncrement;
    private static int size;
    private Vector3[] FFTpositions;
    private double[] oldMagnitudes;
    private double[] rawMagnitudes;
    private double filteredValue;
    private Vector2 auxV2;
    private double totalAverage;
    private IEnumerable<double> Fdelta;
    private IEnumerable<double> Ftheta;
    private IEnumerable<double> Falpha;
    private IEnumerable<double> Fbeta;
    private IEnumerable<double> Fgamma;

    private IEnumerable<double> delta;
    private IEnumerable<double> theta;
    private IEnumerable<double> alpha;
    private IEnumerable<double> beta;
    private IEnumerable<double> gamma;
    private int deltaMax, thetaMin, thetaMax, alphaMin, alphaMax, betaMin, betaMax, gammaMin;

    private float[] bands;

    private float xScale;
    private float yScale;
    private int MaxFreq;
    private float width;
    private float height;

    #endregion
    void Awake()
    {
        manager = transform.GetComponentInParent<FFT_GraphsManager>();
        Aura = manager.Aura;
    }
    void Start()
    {
        bin.Im = 0;
        canCalculateNow = true;
        lR = GetComponent<LineRenderer>();
        rt = GetComponent<RectTransform>();
        width = rt.rect.width;
        height = rt.rect.height;
        N = 256;  //2^n
        binsArray = new Complex[(int)N];
        for (int i = 0; i < binsArray.Length - 1; i++)
        {
            binsArray[i].Re = 0;
            binsArray[i].Im = 0;
        }
        Fs = Aura.inputFrequency;  //Hz
        MaxFreq = 60; //Max =< ( Nyquist = Fs/2 )
        binIncrement = Fs / N;
        size = (int)(MaxFreq / binIncrement);
        size = size + (int)(size * 0.075); // this is for scaling issues
        xScale = MaxFreq / binIncrement;

        deltaMax = (int)((4f * Fs / binIncrement) + 0.5f);
        thetaMin = deltaMax;
        thetaMax = (int)((8f * Fs / binIncrement) + 0.5f);
        alphaMin = thetaMax;
        alphaMax = (int)((14f * Fs / binIncrement) + 0.5f);
        betaMin = alphaMax;
        betaMax = (int)((30f * Fs / binIncrement) + 0.5f);
        gammaMin = betaMax;

        bands = new float[] {0.5f, 4f, 7f, 12f, 30f, 60f};

        FFTpositions = new Vector3[size];
        oldMagnitudes = new double[size];
        rawMagnitudes = new double[size];
        lR.positionCount = size;
        lR.SetPositions(FFTpositions);
        mode = plotScale.Log;
    }

    void Update()
    {
        updateScaling();
        calculate();
        draw();
    }

    #region Private Methods

    private void updateScaling()
    {
        yScale = manager.verticalScale;
    }

    private void calculate()
    {
        if (Aura.running)
        {
            FourierTransform.FFT(binsArray, Direction.Forward);
            separateBands();
            magnitudeArray = binsArray.Select(complex => complex.Magnitude);
            FFTpositions = toFFTpositions(magnitudeArray);
        }
    }

    private Vector3[] toFFTpositions(IEnumerable<double> magnitudes)
    {
        int i = 0;
        auxV2.x = -(rt.rect.width / 2);
        auxV2.y = -(rt.rect.height / 2);
        FFTpositions[i] = auxV2;
        foreach (var magnitude in magnitudes)
        {
            if (i == 0) { i++; continue; } // Ignore mean DC frequency at 0Hz
            if (i >= size) { break; }  // Dont process frequencies beyond the specified in size.
            FFTpositions[i] = getPlot(i, magnitude);
            i++;
        }
        return FFTpositions;
    }

    private void draw()
    {
        if (Aura.running)
        {
            lR.enabled = !lR.enabled; //                                    //
            lR.enabled = !lR.enabled; //Had to make this. Works like a refresh.
            lR.SetPositions(FFTpositions);
        }
    }

    private void separateBands()
    {
        double[] powers = new double[5];
        double[] powers_ratio = new double[5];
        double total_sum = 0;

        for(int i=0; i<bands.Length - 1; i++)
        {
            float freq      = bands[i];
            float next_freq = bands[i+1];
            
            IEnumerable<Complex> r1 = //1750 may changed depending of AURA # of samples
            binsArray.Skip((int) Mathf.Floor(freq/250*1750)).Take((int) Mathf.Floor(next_freq/250*1750));
            
            Complex c1 = new Complex(0, 0);
            foreach(var sum in r1)
            {
                c1 += sum;
            }
            powers[i] = c1.Re;
        }

        foreach(double power in powers)
        {
            total_sum += power;
        }
        
        for(int i=0; i<powers.Length; i++)
        {
            powers_ratio[i] = powers[i] / total_sum;
        }

        for(int i=0; i<powers_ratio.Length; i++)
        {
            switch(i)
            {
                case 0:
                    double[] tmp_delta = new double[1];
                    tmp_delta[0] = powers_ratio[i] * powers_ratio[i] / 4;
                    delta = tmp_delta.Take(1);
                    break;
                case 1:
                    double[] tmp_theta = new double[1];
                    tmp_theta[0] = powers_ratio[i] * powers_ratio[i] / 4;
                    theta = tmp_theta.Take(1);
                    break;
                case 2:
                    double[] tmp_alpha = new double[1];
                    tmp_alpha[0] = powers_ratio[i] * powers_ratio[i] / 4;
                    alpha = tmp_alpha.Take(1);
                    break;
                case 3:
                    double[] tmp_beta = new double[1];
                    tmp_beta[0] = powers_ratio[i] * powers_ratio[i] / 18;
                    beta = tmp_beta.Take(1);
                    break;
                case 4:
                    double[] tmp_gamma = new double[1];
                    tmp_gamma[0] = powers_ratio[i] * powers_ratio[i] / 30;
                    gamma = tmp_gamma.Take(1);
                    break;
            }
        }

    }

    // private void separateBands()
    // {
    //     totalAverage = oldMagnitudes.Average();
    //     Fdelta = oldMagnitudes.Take(4);          //    < 4
    //     Ftheta = oldMagnitudes.Skip(4).Take(4);  //  4 – 8
    //     Falpha = oldMagnitudes.Skip(8).Take(4);  //  8 – 12
    //     Fbeta = oldMagnitudes.Skip(12).Take(18); // 12 - 30
    //     Fgamma = oldMagnitudes.Skip(30);         //    > 30
    // }

    private Vector2 getPlot(int i, double magnitude)
    {
        //rawMagnitudes[i] = magnitude;
        filteredValue = ((magnitude - oldMagnitudes[i]) * (1.0f - manager.getSFac())) + oldMagnitudes[i];
        oldMagnitudes[i] = filteredValue;
        switch (mode)
        {   //Scales are the max values, i.e. last point in the graph
            case plotScale.Linear:
                auxV2.x = ((i * binIncrement) * (1f / xScale)) * rt.rect.width - (rt.rect.width / 2);
                auxV2.y = ((float)filteredValue / yScale) * rt.rect.height - (rt.rect.height / 2);
                return auxV2;
            case plotScale.Log:
                if (filteredValue < 1) { filteredValue = 1; }
                auxV2.x = ((i * binIncrement) * (1f / xScale)) * rt.rect.width - (rt.rect.width / 2);
                auxV2.y = ((float)Math.Log10(filteredValue) / Mathf.Log10(yScale)) * rt.rect.height - (rt.rect.height / 2);
                return auxV2;
            default:
                if (filteredValue < 1) { filteredValue = 1; }
                auxV2.x = ((i * binIncrement) * (1f / xScale)) * rt.rect.width - (rt.rect.width / 2);
                auxV2.y = ((float)Math.Log10(filteredValue) / Mathf.Log10(yScale)) * rt.rect.height - (rt.rect.height / 2);
                return auxV2;
        }
    }

    #endregion

    #region Public Methods

    public void updateFilteredBins(float[] values)
    {
        for (int i = 0; i < binsArray.Length - 1; i++)
        {
            bin.Re = values[values.Length - 1 - i];
            binsArray[binsArray.Length - 1 - i] = bin;
        }
    }

    public IEnumerable<double> getAll() { return oldMagnitudes; }
    public double getAverage() { return totalAverage; }
    public IEnumerable<double> getFDelta() { return Fdelta; }
    public IEnumerable<double> getFTheta() { return Ftheta; }
    public IEnumerable<double> getFAlpha() { return Falpha; }
    public IEnumerable<double> getFBeta() { return Fbeta; }
    public IEnumerable<double> getFGamma() { return Fgamma; }

    public IEnumerable<double> getDeltaNorm() { return delta; }
    public IEnumerable<double> getThetaNorm() { return theta; }
    public IEnumerable<double> getAlphaNorm() { return alpha; }
    public IEnumerable<double> getBetaNorm() { return beta; }
    public IEnumerable<double> getGammaNorm() { return gamma; }

    public double getMin() { return oldMagnitudes.Min(); }
    public double getMax() { return oldMagnitudes.Max(); }

    public void setLinear() { mode = plotScale.Linear; }
    public void setLog() { mode = plotScale.Log; }

    #endregion

}
