﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawIndex : MonoBehaviour {

    private EIGraph_Manager manager;
    private LineRenderer line;
    private RectTransform rt;

    //private Queue<float> indexes = new Queue<float>();
    private GameObject panel;

    private Vector2 aux;
    private Vector3[] positions;
    
    [HideInInspector]
    public double[] indexes;
    private Queue<double> instanceindex = new Queue<double>();

    private int counter = 0;
    private int sizeindex = 0;
    private bool left = false;

    private float oldWidth;


    //private float[] scale;
    private float scaleX;
    private readonly float YMaxLim = 140;

    private readonly float YMinLim = 0;

    private int size = 8;

    private float[] pos;
    //int cpos;

    void Awake()
    {
        manager = transform.GetComponentInParent<EIGraph_Manager>();
        panel = GameObject.Find("EngagementPanel");
    }

    void Start ()
    {
        line = GetComponent<LineRenderer>();
        line.enabled = true;
        rt = GetComponent<RectTransform>();
        //lR.positionCount = size;
        line.enabled = true;

        //indexes = panel.GetComponent<EngagementPanel>().indexes;
        sizeindex = indexes.Length;

        line.positionCount = sizeindex / 8;

        positions = new Vector3[line.positionCount];
        //scale = new float[8];

        float dif = GameObject.Find("Index").GetComponent<RectTransform>().rect.width - 
            GameObject.Find("Index").GetComponent<RectTransform>().rect.x;
        
        scaleX = Mathf.Round(dif) / (line.positionCount);
        Debug.Log(scaleX);


        //AutoScaleX();
        for(int i=0; i<line.positionCount; i++)
        {
            positions[i].x = i * scaleX;
            positions[i].z = -2;
        }

        if (sizeindex > 0) left = true;
        Debug.Log(line.positionCount);
        fillQueue();
        getIndex();
        draw();

        /*Testing the smoothness function*/
        //positions = MakeSmoothCurve(positions, 3.0f);
        //draw();
    }

    /* Usage of MakeSmoothCurve method
    var points : Vector3[];
 
    var lineRenderer : LineRenderer;
    var c1 : Color = Color.yellow;
    var c2 : Color = Color.red;
 
    function Start () 
    {
        points = Curver.MakeSmoothCurve(points,3.0);
        lineRenderer.SetColors(c1, c2);
        lineRenderer.SetWidth(0.5,0.5);
        lineRenderer.SetVertexCount(points.Length);
        var counter : int = 0;
        for(var i : Vector3 in points){
            lineRenderer.SetPosition(counter, i);
            ++counter;
        }
    }
    */

    /*
    private Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve,float smoothness)
    {
        List<Vector3> points;
        List<Vector3> curvedPoints;
        int pointsLength = 0;
        int curvedLength = 0;
         
        if(smoothness < 1.0f) smoothness = 1.0f;
         
        pointsLength = arrayToCurve.Length;
        
        curvedLength = (pointsLength*Mathf.RoundToInt(smoothness))-1;
        curvedPoints = new List<Vector3>(curvedLength);
        
        float t = 0.0f;
        for(int pointInTimeOnCurve = 0;pointInTimeOnCurve < curvedLength+1;pointInTimeOnCurve++){
            t = Mathf.InverseLerp(0,curvedLength,pointInTimeOnCurve);
            
            points = new List<Vector3>(arrayToCurve);
            
            for(int j = pointsLength-1; j > 0; j--){
                for (int i = 0; i < j; i++){
                    points[i] = (1-t)*points[i] + t*points[i+1];
                }
            }
            
            curvedPoints.Add(points[0]);
        }
         
        return(curvedPoints.ToArray());
    }
    */

    private int Next()
    {
        return ((size + 1) % 8);
    }

    private void fillQueue()
    {
        while(counter < sizeindex)
        {
            for(int i = 1; i < 9; i++)
            {
                if(GetNumSeq(i + counter, 2, 8) < sizeindex)
                {
                    instanceindex.Enqueue(indexes[GetNumSeq(i+counter, 2, 8)]);
                }
                else
                {
                    break;
                }
            }
            counter += 8;
        }
    }

    private void getIndex()
    {
        for(int i=0; i< line.positionCount; i++)
        {
            positions[i].y = (float) instanceindex.Dequeue() * 100;
        }
    }

    private void draw()
    {
        line.SetPositions(positions);
    }

    private int GetNumSeq(int an, int a1, int diference)
    {
        return diference * (an - 1) + a1;       /*@param an must start at 1*/
    }
}
