﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Complex = AForge.Math.Complex;

public class DrawIndexV2 : MonoBehaviour {

	private DrawFFT[] fft; //Norm methods have bands normalized as in the library used in python
	private int global_counter;

	private List<double> n_1;
	private List<double> n_2;
	private List<double> n_3;
	private List<double> n_4;
	private List<double> n_5;
	private List<double> n_6;
	private List<double> n_7;
	private List<double> n_8;
	

	void Start () 
	{
		fft = GetComponentsInChildren<DrawFFT>();
		global_counter = 0;

		n_1 = new List<double>();
		n_2 = new List<double>();
		n_3 = new List<double>();
		n_4 = new List<double>();
		n_5 = new List<double>();
		n_6 = new List<double>();
		n_7 = new List<double>();
		n_8 = new List<double>();
	}

	void Update()
	{
		getEngagement(this.fft);
	}
	
	
	public void getEngagement(DrawFFT[] values)
	{

		for(int i=0; i<values.Length; i++)
		{
			double beta, alpha, theta, engagement = 0;

			DrawFFT single_fft = values[i];
			switch(i)
			{
				case 0:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_1.Add(engagement);
					break;
				case 1:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_2.Add(engagement);
					break;
				case 2:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_3.Add(engagement);
					break;
				case 3:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_4.Add(engagement);
					break;
				case 4:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_5.Add(engagement);
					break;
				case 5:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_6.Add(engagement);
					break;
				case 6:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_7.Add(engagement);
					break;
				case 7:
					beta 	= single_fft.getBetaNorm().ElementAt(0);
					alpha 	= single_fft.getAlphaNorm().ElementAt(0);
					theta	= single_fft.getThetaNorm().ElementAt(0);

					engagement = beta / (alpha + theta);
					n_8.Add(engagement);
					break;
			}
		}

	}
	
}
