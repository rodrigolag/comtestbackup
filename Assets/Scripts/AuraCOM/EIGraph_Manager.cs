﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EIGraph_Manager : MonoBehaviour {

    public float verticalScale { get; set; }
    public Aura aura;

    private DrawIndex drawcomponent;
    private LineRenderer lr;
    private float smoothFac = 0;
    private int j = 0;


    // Use this for initialization
    void Start ()
    {
        verticalScale = 1;
        smoothFac = 0.925f;
        drawcomponent = GetComponentInChildren<DrawIndex>();
        lr = GetComponentInChildren<LineRenderer>();
    }

    public void updateSmoothFac(float fac)
    {
        smoothFac = fac;
    }

    public void updateScale(float fac)
    {
        verticalScale = fac;
    }

    public float getSFac()
    {
        return smoothFac;
    }
}
