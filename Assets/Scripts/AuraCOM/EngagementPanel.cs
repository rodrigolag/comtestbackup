﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using SFB;
using System.IO;
using System;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using Tobii.Gaming;
//using LSL;

public class EngagementPanel : MonoBehaviour {

    /*index calculator and Mediaplayer members*/
    public IndexCalculator indexcalculator;
    public Aura aura;
    public MediaPlayer mp;
    private bool init = false;
    private GazeData gazedata;

    private Dropdown dropdown; /*Test the dropdown*/

    public Slider slider;
    private RectTransform sphere;
    int counter = 0;
    private double[] pointerindexes;
    private int sizeindex = 0;
    private LineRenderer linerenderer;

    [HideInInspector]
    public double[] indexes;


    /*gaze data */
    public int PointCloudSize = 10;

    private GazePoint _lastGazePoint = GazePoint.Invalid;

    private GazePoint[] gazePoints;
    private int _last;



    private RectTransform rt, VideoRt;
    private GameObject Panel, VideoPanel, EngagementP;
    private DisplayUGUI display;
    private float rate = 1; //playback rate
    
    [HideInInspector]
    public float vid_length_seconds = 0;
    private string _pathCSV = "", _pathVideo = "";
    private readonly ExtensionFilter[] filtersCSV = new ExtensionFilter[1];
    private readonly ExtensionFilter[] filtersVids = new ExtensionFilter[1];
    private Vector3 open = Vector3.one;
    private Vector3 closed = Vector3.zero;

    private float width, height;

    void Awake()
    {
        Panel = GameObject.Find("Index");
        VideoPanel = GameObject.Find("VideoPanel");
        EngagementP = GameObject.Find("EngagementPanel");
        VideoRt = (VideoPanel) ? VideoPanel.GetComponent<RectTransform>() : null;
        display = GameObject.Find("VideoPanel").GetComponent<DisplayUGUI>();
        display._mediaPlayer = mp;

        //dropdown = GameObject.Find("Nodes").GetComponent<Dropdown>();
        //dropdown.gameObject.SetActive(true);

        slider.enabled = false;
        slider.minValue = 0;
        slider.maxValue = 1;
        slider.value = 0;

        rt = GetComponent<RectTransform>();
        rt.localScale = closed;

        sphere = GameObject.Find("Pointer").GetComponent<RectTransform>();
        linerenderer = GameObject.Find("Index").GetComponent<LineRenderer>();
        Debug.Log("Count positions of line renderer: " + linerenderer.positionCount);
        
    }

    void Start ()
    {
        filtersCSV[0] = new ExtensionFilter("CSV Files", "csv");
        filtersVids[0] = new ExtensionFilter("Video Files", "mp4", "avi", "flv");
        display._mediaPlayer.Events.AddListener(VideoEvent);

        _last = PointCloudSize - 1;

        InitializeGazePointBuffer();

        if (VideoRt)
        {
            Vector2 panelSize = VideoRt.sizeDelta;
            width = panelSize.x;
            height = panelSize.y;
        }
    }

    public void Update()
    {
        /*GazePoint gazePoint = TobiiAPI.GetGazePoint();

        if(gazePoint.IsRecent() && gazePoint.Timestamp > (_lastGazePoint.Timestamp + float.Epsilon))
        {
            if(gazePoint.Screen.x > 0 && gazePoint.Screen.y > 0)
                UpdateGazePointCloud(gazePoint);
        }*/


        //GameObject screen = GameObject.Find("Screen");
        //GameObject gui = GameObject.Find("GUI");
        
        //Text t1 = screen.GetComponent<Text>();
        //Text t2 = gui.GetComponent<Text>();
        
        
        //t1.text = gazePoint.Screen.ToString();
        //t2.text = gazePoint.Viewport.ToString();
        if(slider.enabled)
        {
            slider.value = (display._mediaPlayer.Control.GetCurrentTimeMs() / 1000) / vid_length_seconds;
            int lineValue = (int) Math.Floor(slider.value * pointerindexes.Length);
            if(pointerindexes.Length > 0 && lineValue < linerenderer.positionCount)
            {
                sphere.localPosition = new Vector3(linerenderer.GetPosition(lineValue).x, 
                            linerenderer.GetPosition(lineValue).y, -1);
                
            }
            int heatValue = (int) Math.Floor(slider.value * Heatmap.heatpoints.Length);
            if(GameObject.Find("heatmap").GetComponent<Heatmap>().enabled 
                && heatValue < Heatmap.heatpoints.Length)
            {
                if(heatValue % 2 == 0)
                    Heatmap._last = heatValue;
                else
                    Heatmap._last = heatValue - 1;
            }
        }

    }


    /*Slider*/
    public void checkValue()
    {
        display._mediaPlayer.Control.Seek(slider.value * vid_length_seconds * 1000);
        //Debug.Log(slider.value);
    }

      public void OpenVideoFile()
    {
        SaveResultVideo(StandaloneFileBrowser.OpenFilePanel("Open File", "", filtersVids, false));
        display._mediaPlayer.m_VideoPath = _pathVideo;
    }

    public void OpenCSVFile()
    {
        SaveResultCSV(StandaloneFileBrowser.OpenFilePanel("Open File", "", filtersCSV, false));

        if(_pathCSV.Length != 0) sendPath();
        
    }

    private Vector2[] FloatToVector2(float[] points)
    {
        Vector2[] v = new Vector2[points.Length / 2];
        int j = 0;

        for(int i = 0; i < points.Length; i += 2)
        {
            v[j].x = points[i];
            v[j++].y = points[i+1];
        }

        /*foreach(Vector2 vec in v)
        {
            Debug.Log(vec.x + ", " + vec.y);
        }*/

        return v;
    }

    private string GetFile(string filename)
    {
        try
        {
            string file = "";
            string sDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var directory = new DirectoryInfo(sDir);

            file = directory.GetFiles(filename)[0].FullName;
            file = file.Replace('\\', '/');
            return file;
            /*foreach(FileInfo curFile in directory.GetFiles(filename))
            {
                file = curFile.FullName;
                file = file.Replace('\\', '/');
                Debug.Log(file);
            }*/

        }catch(Exception e)
        {
            Debug.Log("File not found"); 
            return null;
        }
    }

    private void DeSerialize(string file)
    {
        if(File.Exists(file))
        {
            string dataJSON = File.ReadAllText(file);
            gazedata = JsonUtility.FromJson<GazeData>(dataJSON);
        }
        else
        {
            gazedata = new GazeData();
        }
    }

    private void UpdateGazePointCloud(GazePoint point)
    {
        _last = Next();
        gazePoints[_last] = point;
    }


    private int Next()
    {
        return ((_last + 1) % PointCloudSize);
    }

    private void InitializeGazePointBuffer()
    {
        gazePoints = new GazePoint[PointCloudSize];
        for (int i = 0; i < PointCloudSize; i++)
        {
            gazePoints[i] = GazePoint.Invalid;
        }
    }

    public void PlayVideo()
    {
        if(_pathVideo.Length != 0)
        {
            if(display._mediaPlayer.Control.IsPaused())
            {
                display._mediaPlayer.Control.Play();
            }
            else if(display._mediaPlayer.Control.IsFinished())
            {
                Debug.Log("Finished");
                //display._mediaPlayer.Control.Seek(0);
                //display._mediaPlayer.Control.Play();
                slider.value = 0;
                slider.enabled = true;
                if(gazedata.gazedata.Length != 0)
                {
                    Vector2[] v = FloatToVector2(gazedata.gazedata);
                    Heatmap.heatpoints = v;
                    GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = true;
                    GameObject.Find("Index").GetComponent<DrawIndex>().enabled = true;
                }
            }
            else if(display._mediaPlayer.Control.IsPlaying()) {/*Have to fix this...*/}
            else
            {
                if(!init)
                {
                    init = true;
                    display._mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, _pathVideo, true);
                    GameObject.Find("Index").GetComponent<DrawIndex>().indexes = this.indexes;
                    GameObject.Find("Index").GetComponent<DrawIndex>().enabled = true;
                    string file = GetFile("data.json");
                    

                    DeSerialize(file);
                    if(gazedata.gazedata.Length != 0)
                    {
                        Vector2[] v = FloatToVector2(gazedata.gazedata);
                        Heatmap.heatpoints = v;
                        GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = true;
                    }
                }
            }
        }
        else
        {
            GameObject.Find("Index").GetComponent<DrawIndex>().indexes = this.indexes;
            GameObject.Find("Index").GetComponent<DrawIndex>().enabled = true;
        }
    }

    public void PauseVideo()
    {
        if(_pathVideo.Length != 0)
        {
            if(slider.value != slider.maxValue)
            {
                if(display._mediaPlayer.Control.IsPlaying())
                {
                    display._mediaPlayer.Control.Pause();
                }
            }
        }
    }

    public void ForwardVideo()
    {   
        if(rate < 2.25)
        {
            rate += .25f;
            display._mediaPlayer.Control.SetPlaybackRate(rate);
        }
    }

    public void RewindVideo()
    {   
        if(rate > 0)
        {
            rate -= .25f;
            display._mediaPlayer.Control.SetPlaybackRate(rate);
        }
    }

    public void sendPath()
    {
        indexes = indexcalculator.getEngagementF1(_pathCSV, 500);
        fillQueue();
    }

    private void fillQueue()
    {
        Queue<double> instanceindex = new Queue<double>();
        int size = 0;
        sizeindex = indexes.Length;
        while(counter < sizeindex)
        {
            for(int i = 1; i < 9; i++)
            {
                if(GetNumSeq(i + counter, 2, 8) < sizeindex)
                {
                    instanceindex.Enqueue(indexes[GetNumSeq(i+counter, 2, 8)]);
                    size++;
                }
                else
                {
                    break;
                }
            }
            counter += 8;
        }

        pointerindexes = new double[size];
        counter = 0;

        while(instanceindex.Count > 0)
        {
            pointerindexes[counter++] = instanceindex.Dequeue();
        }
        counter = 0;
    }

    private void SaveResultVideo(string[] paths)
    {
        if (paths.Length == 0)
        {
            return;
        }

        _pathVideo = "";
        foreach (var p in paths)
        {
            _pathVideo = Path.Combine(_pathVideo, p);
        }
    }

    private void SaveResultCSV(string[] paths)
    {
        if (paths.Length == 0)
        {
            return;
        }

        _pathCSV = "";
        foreach (var p in paths)
        {
            _pathCSV = Path.Combine(_pathCSV, p);
        }
    }

    public void VideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode err)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.ReadyToPlay:
                mp.Control.Play();
                break;
            case MediaPlayerEvent.EventType.FirstFrameReady:
                /*Debug.Log("First frame ready");*/
                /*Debug.Log("Length of video");*/
                /*Debug.Log(mp.Info.GetDurationMs());*/
                vid_length_seconds = mp.Info.GetDurationMs() / 1000;
                slider.enabled = true;
                break;
            case MediaPlayerEvent.EventType.Closing:
                display._mediaPlayer.m_VideoPath = "";
                //_pathVideo = "";
                if(GameObject.Find("heatmap").GetComponent<Heatmap>().enabled)
                {
                    //Array.Clear(Heatmap.heatpoints, 0, Heatmap.heatpoints.Length);
                    Heatmap.heatpoints = null;
                    GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = false;
                }
                slider.enabled = false;
                init = false;
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                display._mediaPlayer.m_VideoPath = "";
                //_pathVideo = "";
                if(GameObject.Find("heatmap").GetComponent<Heatmap>().enabled)
                {
                    //Array.Clear(Heatmap.heatpoints, 0, Heatmap.heatpoints.Length);
                    Heatmap.heatpoints = null;
                    GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = false;
                }
                slider.enabled = false;
                init = false;
                GameObject.Find("Index").GetComponent<DrawIndex>().enabled = false;
                /*mp.CloseVideo();*/
                break;
        }
    }

    public void Activate()
    {
        rt.localScale = open;
    }

    public void deActivate()
    {
        rt.localScale = closed;
        _pathVideo = "";
        display._mediaPlayer.CloseVideo();
        Panel.GetComponent<LineRenderer>().enabled = false;
        GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = false;
        slider.value = 0;
        slider.enabled = false;
    }

    /*Filer channel values*/
    private int GetNumSeq(int an, int a1, int diference)
    {
        /*@param 'an' must start at 1*/
        return diference * (an - 1) + a1;
    }

}
