﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class FFT_Band_Intensities : MonoBehaviour
{
    #region Public Variables
    [HideInInspector]
    public BandMode mode;
    public enum BandMode { All, Delta, Theta, Alpha, Beta, Gamma }

    public IEnumerable<string> deltasMax;
    public IEnumerable<string> thetasMax;
    public IEnumerable<string> alphasMax;
    public IEnumerable<string> betasMax;
    public IEnumerable<string> gammasMax;

    public IEnumerable<double> deltasMaxf;
    public IEnumerable<double> thetasMaxf;
    public IEnumerable<double> alphasMaxf;
    public IEnumerable<double> betasMaxf;
    public IEnumerable<double> gammasMaxf;

    #endregion

    #region Private Variables

    private Aura Aura;
    private DrawFFT[] FFTchannels;
    private IEnumerable<float> means;
    private IEnumerable<float> minimums;
    private IEnumerable<float> maximums;
    private IEnumerable<float> intensities;
    private bool calculated;

    #endregion

    void Awake()
    {
        Aura = transform.GetComponentInParent<Aura>();
    }

    void OnEnable()
    {
        startup();
    }

    void Start()
    {
        startup();
    }

    private void startup()
    {
        mode = BandMode.All;
        calculated = false;
        FFTchannels = Aura.FFT_GraphsManager.GetComponentsInChildren<DrawFFT>();
    }

    void Update()
    {
        if (Aura.running)
        {
            calculateIntensities();
        }
    }

    #region Private Methods

    private void calculateIntensities()
    {
        maximums = FFTchannels.Select(component => (float)component.getMax());

        deltasMaxf = FFTchannels.Select(channel => ((getFilteredBand(BandMode.Delta, channel)).Max()));
        thetasMaxf = FFTchannels.Select(channel => ((getFilteredBand(BandMode.Theta, channel)).Max()));
        alphasMaxf = FFTchannels.Select(channel => ((getFilteredBand(BandMode.Alpha, channel)).Max()));
        betasMaxf = FFTchannels.Select(channel => ((getFilteredBand(BandMode.Beta, channel)).Max()));
        gammasMaxf = FFTchannels.Select(channel => ((getFilteredBand(BandMode.Gamma, channel)).Max()));

        deltasMax = deltasMaxf.Select(values => values.ToString("0.00"));
        thetasMax = thetasMaxf.Select(values => values.ToString("0.00"));
        alphasMax = alphasMaxf.Select(values => values.ToString("0.00"));
        betasMax  = betasMaxf.Select(values => values.ToString("0.00"));
        gammasMax = gammasMaxf.Select(values => values.ToString("0.00"));

        intensities = FFTchannels.Select(channel => (float)(getFilteredBand(mode, channel).Max() / maximums.Max() ));

        calculated = true;
    }
    private double GetAverage(BandMode bandmode, DrawFFT fft)
    {
        switch (bandmode)
        {
            case BandMode.All:      return fft.getAverage();
            case BandMode.Delta:    return fft.getFDelta().Average();
            case BandMode.Theta:    return fft.getFTheta().Average();
            case BandMode.Alpha:    return fft.getFAlpha().Average();
            case BandMode.Beta:     return fft.getFBeta().Average();
            case BandMode.Gamma:    return fft.getFGamma().Average();
            default: return fft.getAverage();
        }
    }

    private IEnumerable<double> getFilteredBand(BandMode bandmode, DrawFFT fft)
    {
        switch (bandmode)
        {
            case BandMode.All:      return fft.getAll();
            case BandMode.Delta:    return fft.getFDelta();
            case BandMode.Theta:    return fft.getFTheta();
            case BandMode.Alpha:    return fft.getFAlpha();
            case BandMode.Beta:     return fft.getFBeta();
            case BandMode.Gamma:    return fft.getFGamma();
            default: return fft.getAll();
        }
    }

    #endregion


    #region Public Methods

    public void setAll() { mode = BandMode.All; }

    public void setDelta() { mode = BandMode.Delta; }

    public void setTheta() { mode = BandMode.Theta; }

    public void setAlpha() { mode = BandMode.Alpha; }

    public void setBeta() { mode = BandMode.Beta; }

    public void setGamma() { mode = BandMode.Gamma; }

    public bool isCalculated() { return calculated; }

    public float[] getIntensities() { return intensities.ToArray(); }

    #endregion

}