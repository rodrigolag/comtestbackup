﻿using UnityEngine;
using UnityEngine.UI;


public class FFT_GraphsManager : MonoBehaviour
{
    public Aura Aura;
    public float verticalScale { get; set; }

    private DrawFFT[] drawComponents;
    private LineRenderer[] LRs;
    private float smoothFac = 0;
    private int j = 0;

    void Start()
    {
        verticalScale = 5;
        smoothFac = 0.925f;
        drawComponents = GetComponentsInChildren<DrawFFT>();
        LRs = GetComponentsInChildren<LineRenderer>();
    }

    public void setScale(int mode)
    {
        switch (mode)
        {
            case 0:
                foreach (DrawFFT drawing in drawComponents) { drawing.setLog(); }
                break;
            case 1:
                foreach (DrawFFT drawing in drawComponents) { drawing.setLinear(); }
                break;
            default:
                foreach (DrawFFT drawing in drawComponents) { drawing.setLog(); }
                break;
        }
    }

    public void updateSmoothFac(float fac)
    {
        smoothFac = fac;
    }

    public void updateScale(float fac)
    {
        verticalScale = fac;
    }

    public float getSFac()
    {
        return smoothFac;
    }

}

