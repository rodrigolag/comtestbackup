﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading;
using CielaSpike;
using LitJson;

public class HTTP : MonoBehaviour {

    //GSR communication variables
    //private const string URL = "http://localhost:22002/NeulogAPI?";
    private const string URL = "http://localhost:22002/NeuLogAPI?GetSensorValue:[GSR],[1]";
    private string commands = "";
    private string parameters = "";
    private const string SENSOR = "GSR"; //"[GSR] --> better"
    private Thread thread;
    private Task task;

    private void Start()
    {
        WWW request = new WWW("http://localhost:22002/NeuLogAPI?GetSensorValue:[GSR],[1]");
        StartCoroutine(Command(request));
    }

    //public void StartGSR()
    //{
    //    thread = new Thread(Command);
    //    thread.Start();
    //}

    //private void Command()
    //{
    //    WWW request = new WWW(URL);

    //}


    IEnumerator Command(WWW req)
    {
        yield return req;

        var data = LitJson.JsonMapper.ToObject<GSRValue>(req.text);
        Debug.Log(data.GetSensorValue[0]);
    }


    private class GSRValue
    {
        public double[] GetSensorValue { get; set; }
    }

}
