﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using Complex = AForge.Math.Complex;
using FourierTransform = AForge.Math.FourierTransform;
using Direction = AForge.Math.FourierTransform.Direction;

public class IndexCalculator : MonoBehaviour {

    //beta / (alpha + theta)

    public double[][]getEngagement(string csv_file)
    {
        double[][] engagement = new double[8][];
        double[][] nodes = new double[8][];

         /*Load csv to matrix nodes*/
            using (var reader = new StreamReader(csv_file))
            {
                double[] node_o1;
                double[] node_o2;
                double[] node_p7;
                double[] node_fz;
                double[] node_p4;
                double[] node_p3;
                double[] node_pz;
                double[] node_cz;

                List<string> o1 = new List<string>();
                List<string> o2 = new List<string>();
                List<string> p7 = new List<string>();
                List<string> fz = new List<string>();
                List<string> p4 = new List<string>();
                List<string> p3 = new List<string>();
                List<string> pz = new List<string>();
                List<string> cz = new List<string>();

                /*Skip first row (headers)*/
                string h = reader.ReadLine();
                string[] headers = h.Split(',');
                int counter = 0;

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    o1.Add(values[1]);
                    o2.Add(values[2]);
                    p7.Add(values[3]);
                    fz.Add(values[4]);
                    p4.Add(values[5]);
                    p3.Add(values[6]);
                    pz.Add(values[7]);
                    cz.Add(values[8]);
                }

                node_o1 = transformToFloatArray(o1);
                node_o2 = transformToFloatArray(o2);
                node_p7 = transformToFloatArray(p7);
                node_fz = transformToFloatArray(fz);
                node_p4 = transformToFloatArray(p4);
                node_p3 = transformToFloatArray(p3);
                node_pz = transformToFloatArray(pz);
                node_cz = transformToFloatArray(cz);

                for(int i=0; i<8; i++)
                {
                    nodes[i] = new double[node_o1.Length];
                }

                nodes[0] = node_o1;
                nodes[1] = node_o2;
                nodes[2] = node_p7;
                nodes[3] = node_fz;
                nodes[4] = node_p4;
                nodes[5] = node_p3;
                nodes[6] = node_pz;
                nodes[7] = node_cz;
            }

        return engagement;
    }

    private static double[] transformToFloatArray(System.Object y)
    {
        if (y.GetType() != typeof(List<string>)) return null;

        List<string> arr = (List<string>)y;
        
        double[] transformedArray = new double[arr.Count];
        int c = 0;
        foreach (string s in arr)
        {
            transformedArray[c++] = double.Parse(s, System.Globalization.NumberStyles.Any);
        }
        return transformedArray;
    }

    public static void SaveToCSV(string filename, ArrayList arr)
    {
        string strSeparator = ",";
        StringBuilder output = new StringBuilder();

        int len = arr.Count;

        double[] tmp = new double[len];
        string[] values = new string[len];

        for (int i = 0; i < len; i++)
        {
            tmp[i] = (double)arr[i];
            values[i] = tmp[i].ToString();
        }

        for (int i = 0; i < len; i++)
            output.AppendLine(string.Join(strSeparator, values));
            //output.AppendLine(string.Join(strSeparator, tmp[i]));

        File.WriteAllText(filename, output.ToString());
    }
    public double[] getEngagementF1(string file, int miliseconds)
    {

        //this dont change in size
        string[] lines = File.ReadAllLines(file);
        double[] ratios = new double[8 * 5];                //ratios per each node every measure read   
        string[] fields;                                    //measures

        int end = lines.Length;
        double[] forMeanRatio = new double[8];              //sums the ratios of ratios array for mean later
        double[] realEI = new double[8];                    //here is the real engagement index for each
                                                            //secondUpdate given in params
                                                            //not final yet
        ArrayList savedIndex = new ArrayList();             //stores the engagements of realEI array such that
                                                            //each array of this array establishes an EI for
                                                            //each secondUpdate. Each entry of the arrays,
                                                            //then referes to the EI of each node

        //this need to be initialize everytime a new index has to be calculated
        int ratiosIndex = 0;
        bool secondUpdateMet = false;

        //this need to be initialize just once, then is responsability of the loop to initialize with other values
        int startPoint = 3;                                 //begin to count the timestamps
        int endPoint = startPoint + 1;                      //keep track of last timestamp to meet the 
                                                            //secondUpdate param

        string referenceDate;
        DateTime refDate;

        while (endPoint < end - 3)
        {
            fields = lines[startPoint].Split(new char[] { ',' });
            referenceDate = fields[0].Substring(2, fields[0].Length - 4);
            refDate = DateTime.ParseExact(referenceDate, "HH:mm:ss.fff dd/MM/yyyy", null);

            //this while loop is to know where does the secondUpdate occurs (how many measures are needed to read)
            while (!secondUpdateMet)
            {
                fields = lines[endPoint].Split(new char[] { ',' });
                string cleanDate = fields[0].Substring(2, fields[0].Length - 4);
                DateTime date = DateTime.ParseExact(cleanDate, "HH:mm:ss.fff dd/MM/yyyy", null);

                if ((Math.Abs(date.Millisecond - refDate.Millisecond)) >= miliseconds)
                    secondUpdateMet = !secondUpdateMet;
                
                if (endPoint > end - 3)
                    secondUpdateMet = !secondUpdateMet;
                
                endPoint++;
            }

            int dif = endPoint - startPoint;

            for (int j = startPoint; j < endPoint; j++)
            {
                fields = lines[j].Split(new char[] { ',' });
                for (int i = 0; i < 5; i++)
                {
                    int sites = 1;
                    while (sites < 9)
                    {
                        ratios[ratiosIndex] = double.Parse(fields[(8 * i) + sites]);
                        sites++;
                        ratiosIndex++;
                    }
                }
                ratiosIndex = 0;
                int bands = 4;

                //calculation of engagement index in this loop
                while (ratiosIndex < 8)
                {
                    double up = ratios[(8 * --bands) + ratiosIndex];
                    double left = ratios[(8 * --bands) + ratiosIndex];
                    double right = ratios[(8 * --bands) + ratiosIndex];
                    //double left = ratios[(8 * --bands) + ratiosIndex];
                    //double right = 0;
                    if (left + right == 0)
                    {
                        bands = 4;
                        ratiosIndex++;
                        continue;
                    }
                    forMeanRatio[ratiosIndex] += up / (left + right);
                    ratiosIndex++;
                    bands = 4;
                }

                ratiosIndex = 0;
            }

            for (int i = 0; i < 8; i++)
            {
                if (forMeanRatio[i] == 0 || double.IsNaN(forMeanRatio[i]) || double.IsInfinity(forMeanRatio[i]))
                    continue;
                realEI[i] = forMeanRatio[i] / dif;
            }


            savedIndex.AddRange(realEI);

            for (int i = 0; i < forMeanRatio.Length; i++)
                forMeanRatio[i] = 0;
            

            startPoint = endPoint;
            endPoint = startPoint + 1;
            secondUpdateMet = !secondUpdateMet;
        }

        return (Double[])savedIndex.ToArray(typeof(Double));
    }


}
