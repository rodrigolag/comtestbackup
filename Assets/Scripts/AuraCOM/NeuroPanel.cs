﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NeuroPanel : MonoBehaviour {

	private Vector3 open = Vector3.one;
    private Vector3 closed = Vector3.zero;

	private RectTransform rt;

	void Awake()
	{
		rt = GetComponent<RectTransform>();
        rt.localScale = closed;
	}

	public void Activate()
    {
        rt.localScale = open;
    }

    public void deActivate()
    {
        rt.localScale = closed;
    }
}
