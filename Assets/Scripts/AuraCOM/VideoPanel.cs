﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using SFB;
using System.IO;
using Tobii.Gaming;
using System;
//using UnityEditor;


public class VideoPanel : MonoBehaviour
{
    
    public MediaPlayer mp;

    private DisplayUGUI display;

    Button playbtn;

    CSV_recorder recorder;

    private RectTransform rt;

    private string _path = "";
    bool passed;
    private readonly ExtensionFilter[] filtersVids = new ExtensionFilter[1];
    private String[] extensionFiles;

    private Vector3 open = Vector3.one;
    private Vector3 closed = Vector3.zero;

    //Gaze members
    private GazePoint _lastGazePoint = GazePoint.Invalid;
    private ArrayList gazepoints;
    private const float MaxVisibleDurationInSeconds = 0.5f;
    private GazeData gazedata;

    //Recorder members
    private string MyDocsPath;
    private string GazePath;
    private string DateFormatGazeData;
    private string fileNameDateString;
    private DateTime now;
    private bool present = false;   /*is Tobii connected*/


    private void Awake()
    {
        rt = GetComponent<RectTransform>();
        rt.localScale = closed;
        playbtn = GameObject.Find("Open").GetComponent<Button>();
        playbtn.enabled = false;
        recorder = GameObject.Find("Recorder").GetComponent<CSV_recorder>();
        display = GameObject.Find("VideoPanelR").GetComponent<DisplayUGUI>();
        display._mediaPlayer = mp;
        gazepoints = new ArrayList();
        gazedata = new GazeData();
    }

    void Start()
    {
        filtersVids[0] = new ExtensionFilter("Video Files", "mp4", "avi", "flv");
        extensionFiles = new string[filtersVids[0].Extensions.Length];
        extensionFiles[0] = ".mp4";
        extensionFiles[1] = ".avi";
        extensionFiles[2] = ".flv";
        display._mediaPlayer.Events.AddListener(VideoEvent);
        DateFormatGazeData = "yyyy-MM-dd___HH;mm;ss";
        passed = false;

        TobiiAPI.SubscribeGazePointData();
    }

    void Update()
    {
        if (mp.Control.IsPlaying())
        {

            GazePoint gazePoint = TobiiAPI.GetGazePoint();
            if (gazePoint.IsRecent() && gazePoint.Timestamp > (_lastGazePoint.Timestamp + float.Epsilon) && gazePoint.IsValid)
            {
                Vector2 videoHeat = new Vector2(gazePoint.Viewport.x, gazePoint.Viewport.y * 0.92f); /*For the video panel in Unity */
                Vector2 engHeat = new Vector2(videoHeat.x * .86f, .26f + videoHeat.y * .74f);   /*Same for the heatmap inside the panel */
                gazepoints.Add(engHeat);
            }

            _lastGazePoint = gazePoint;
        }
    }

    private void Serialize(string filename, GazeData s)
    {
        string dataJSON = JsonUtility.ToJson(s);
        File.WriteAllText(filename, dataJSON);
    }

    /*private void SaveGazeData()
    {
        if (gazepoints.Count != 0)
        {
            MyDocsPath = GetMyDocumentsPath();
            fileNameDateString = now.ToString(DateFormatGazeData);
            MyDocsPath = Path.Combine(MyDocsPath, fileNameDateString);
            Directory.CreateDirectory(MyDocsPath);
            GazePath = Path.Combine(MyDocsPath, "GAZE_DATA___" + fileNameDateString + ".json");

            gazedata.gazedata = new float[gazepoints.Count * 2];
            int i = 0;

            foreach(Vector2 v in gazepoints)
            {
                gazedata.gazedata[i++] = v.x;
                gazedata.gazedata[i++] = v.y;
            }

            for (int i = 0; i < gazes.gazedata.Length; i += 2)
            {
                for(int j = i; j < j+2; j++)
                {
                    gazes.gazedata[j] = 
                }
                gazes.gazedata[i] = (float)gazepoints[i];
            }
            //Serialize("C:\\Users\\roris\\Documents\\data.sav", gazes);
            WriteToCSV("C:\\Users\\roris\\Documents\\COMtest\\Assets\\StreamingAssets\\data.csv", gazedata);
        }
    }*/

    private string GetMyDocumentsPath()
    {

        string p = "";
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            p = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            p = Path.Combine(p, "Gaze Records");
            try { Directory.CreateDirectory(p); }
            catch (Exception)
            {
                Debug.Log("Error saving on MyDocuments, saving on internal folder:");
                p = Application.streamingAssetsPath;
                Debug.Log(p);
                Debug.Log("(Administrator Rights needed)");
            }
        }
        else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            p = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            p = Path.Combine(p, "Gaze Records");
            try { Directory.CreateDirectory(p); }
            catch (Exception)
            {
                Debug.Log("Error saving on MyDocuments, saving on internal folder:");
                p = Application.streamingAssetsPath;
                Debug.Log(p);
                Debug.Log("( Administrator Rights needed )");
            }
        }
        else
        {
            p = Application.streamingAssetsPath;
        }
        return p;

    }

    public void OpenFile()
    {
        SaveResultVideo(StandaloneFileBrowser.OpenFilePanel("Open File", "", filtersVids, false));
        display._mediaPlayer.m_VideoPath = _path;

        foreach(string s in extensionFiles)
        {
            if (_path.Contains(s))
            {
                passed = true;
                break;
            }
            else
            {
                passed = false;
            }
        }
        
        if(_path.Length != 0 && passed)
            playbtn.enabled = true;
    }

    public void PlayVideo()
    {
        //GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = true;
        recorder.recording = true;
        display._mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, this._path, true);
    }

    private void SaveResultVideo(string[] paths)
    {
        if (paths.Length == 0)
        {
            return;
        }

        _path = "";
        foreach (var p in paths)
        {
            _path = Path.Combine(_path, p);
        }
    }

    private void VideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode err)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.ReadyToPlay:
                mp.Control.Play();
                break;
            case MediaPlayerEvent.EventType.FirstFrameReady:
                //Debug.Log("First frame ready");
                break;
            case MediaPlayerEvent.EventType.Closing:
                display._mediaPlayer.m_VideoPath = "";
                _path = "";
                recorder.recording = false;
                //SaveGazeData();
                //Debug.Log("Saved points");
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                display._mediaPlayer.m_VideoPath = "";
                _path = "";
                recorder.recording = false;
                mp.CloseVideo();
                //SaveGazeData();
                //Debug.Log("Saved points");
                break;
        }
    }

    public void activate()
    {
        rt.localScale = open;
    }

    public void deActivate()
    {
        display._mediaPlayer.CloseVideo();
        rt.localScale = closed;
        //GameObject.Find("heatmap").GetComponent<Heatmap>().enabled = false;
        GazePoint gazepoint = TobiiAPI.GetGazePoint();
        if(gazepoint.IsRecent()) present = true;
        
        if(present)
        {
            Vector2[] v = gazepoints.ToArray(typeof(Vector2)) as Vector2[];
            gazedata.gazedata = new float[v.Length * 2];
            int counter = 0;
            foreach(Vector2 vec in v)
            {
                gazedata.gazedata[counter++] = vec.x;
                gazedata.gazedata[counter++] = vec.y;
            }
            //Heatmap.heatpoints = v;   //pass direct buffer from memory
            Serialize("C:\\Users\\roris\\Documents\\data.json", gazedata);
            Debug.Log("saved json");
        }
    }

    private Vector2[] FloatToVector2(float[] points)
    {
        Vector2[] v = new Vector2[points.Length / 2];
        int j = 0;

        for(int i = 0; i < points.Length; i += 2)
        {
            v[j].x = points[i];
            v[j].y = points[i+1];
        }

        foreach(Vector2 vec in v)
        {
            Debug.Log(vec.x + ", " + vec.y);
        }

        return v;
    }
}
