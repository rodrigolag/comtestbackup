﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Linq;

public class qualityManager : MonoBehaviour
{

    internal bool on { get; set; }

    private Image image;
    private Color good;
    private Color medium;
    private Color bad;
    private Color noSig;
    private const int limit = 10;
    private int blueWeight = 0;
    private int redWeight = limit;

    // Use this for initialization
    void Start()
    {
        image = gameObject.transform.GetChild(0).gameObject.GetComponent<Image>();

        good = new Color32(0x3D, 0xB4, 0xE7, 0xFF);
        medium = new Color32(0xFF, 0xD6, 0x59, 0xFF);
        bad = new Color32(0xE5, 0x48, 0x58, 0xFF);
        noSig = new Color32(0x78, 0x78, 0x78, 0xFF);

        image.color = bad;
    }

    void Update()
    {
        if (image != null)
        {
            if (blueWeight > redWeight)
            {
                image.color = good;
                on = true;
            }
            else
            {
                image.color = bad;
                on = false;
            }
        }
    }

    public void changeColor(bool red) //Implemented some sort of filter
    {
        if (red)
        {
            blueWeight -= 1;
            if (blueWeight < 0) blueWeight = 0;
            redWeight += 1;
            if (redWeight > limit) redWeight = limit;
        }
        else
        {
            blueWeight += 1;
            if (blueWeight > limit) blueWeight = limit;
            redWeight -= 1;
            if (redWeight < 0) redWeight = 0;
        }
    }


}