﻿using UnityEngine;
using System.Collections;

public class AutoScaleWindow : MonoBehaviour {

    private RectTransform rt;
    private Vector3 v3;

    void Start()
    {
        rt = GetComponentInParent<RectTransform>();
        v3.z = 1;
    }
	
	// Update is called once per frame
	void Update () {
        v3.x = rt.rect.width;
        v3.y = rt.rect.height;
        transform.localScale = v3;
	}
}