﻿using UnityEngine;
using System.Collections;
using ReadWriteCsv;
using System;
using System.Linq;
using UnityEngine.UI;
using csmatio.types;
using System.Collections.Generic;
using csmatio.io;
using CielaSpike;
using System.IO;

public class CSV_recorder : MonoBehaviour
{
    public Aura Aura;
    public GameObject Graphs;
    public bool recording { get; set; }

    private double[][] MatlabData;
    private FFT_Band_Intensities intensities;
    private DateTime now;
    private DateTime EEGtimeRec;
    private DateTime FFTtimeRec;
    private Text aux;
    private CsvFileReader reader;
    private int increment;
    private int counter;
    private bool saveNextFilteredFrame;
    private string EEGdateString;
    private string FFTdateString;
    private string fileNameDateString;
    private string MATdateString;
    private string DateFormatForCSVFile;
    private string DateFormatForExplorerFile;
    private string DateFormatForMatlabFile;
    private string EEGpath;
    private string FFTpath;
    private string FilteredPath;
    private string MatlabFilepath;
    private string MyDocsPath;

    private string[] names = new string[8];
    private Task task;

    public static bool doneWritingMatlabFile = true;
    public static bool canAddMore = true;
    public static bool EEGrefresh = false;


    void Awake()
    {
        intensities = Aura.GetComponentInChildren<FFT_Band_Intensities>();
    }

    // Use this for initialization
    void Start()
    {
        recording = false;
        counter = 1;
        increment = 4;
        DateFormatForCSVFile = "[HH:mm:ss.fff dd/MM/yyyy]";
        DateFormatForExplorerFile = "yyyy-MM-dd___HH;mm;ss";
        DateFormatForMatlabFile = "yyyy_MM_dd_HH_mm_ss";

        StartCoroutine(Raw_File());

    }


    IEnumerator Raw_File()
    {
        while (true)
        {
            if (recording && Aura.running)
            {
                doneWritingMatlabFile = false;
                now = DateTime.Now;
                EEGtimeRec = now.AddSeconds(-now.Second);
                EEGtimeRec = EEGtimeRec.AddMilliseconds(-now.Millisecond);
                EEGdateString = EEGtimeRec.ToString(DateFormatForCSVFile);
                fileNameDateString = now.ToString(DateFormatForExplorerFile);
                MATdateString = now.ToString(DateFormatForMatlabFile);

                for (int n = 0; n < 8; n++)
                {
                    aux = Graphs.transform.GetChild(n).transform.GetChild(2).GetComponent<Text>(); //GetChild(3) is the dynamic label
                    if (aux.IsActive())
                    {
                        names[n] = "'" + aux.text + "'";
                    }
                    else
                    {
                        aux = Graphs.transform.GetChild(n).transform.GetChild(1).GetComponent<Text>();  //GetChild(2) is the channel number label
                        names[n] = "'" + aux.text + "'";
                    }
                }
                MyDocsPath = GetMyDocumentsPath();
                MyDocsPath = Path.Combine(MyDocsPath, fileNameDateString);
                Directory.CreateDirectory(MyDocsPath);
                EEGpath = Path.Combine(MyDocsPath, "AURA_RAW___" + fileNameDateString + ".csv");
                MatlabFilepath = Path.Combine(MyDocsPath, "AURA_RAW___" + fileNameDateString + ".mat");

                StartCoroutine(FFT_File());
                StartCoroutine(Filtered_File());

                using (CsvFileWriter writer = new CsvFileWriter(EEGpath))
                {
                    CsvRow row = new CsvRow();
                    row.Add("'Time and date'");
                    for (int n = 0; n < 8; n++)
                    {
                        row.Add(names[n]);
                    }
                    row.Add("'AccX'");
                    row.Add("'AccY'");
                    row.Add("'AccZ'");
                    row.Add("'GyroX'");
                    row.Add("'GyroY'");
                    row.Add("'GyroZ'");
                    row.Add("'Battery'");
                    row.Add("'Event'");
                    writer.WriteRow(row);
                    row.Clear();
                    row.Add("'[hh:mm:ss.mmm dd/mm/yyyy]'"); //* []
                    for (int i = 0; i < 8; i++)
                    {
                        row.Add("'uV'"); //Add this 8 times
                    }
                    for (int i = 0; i < 3; i++)
                    {
                        row.Add("'G'"); //Add this 3 times
                    }
                    for (int i = 0; i < 3; i++)
                    {
                        row.Add("'Deg/s'"); //Add this 3 times
                    }
                    row.Add("'%'");
                    row.Add("'Button'");
                    writer.WriteRow(row);
                    row.Clear();
                    while (recording && Aura.streaming)
                    {
                        if (EEGrefresh)
                        {
                            for (int i = 0; i < 9; i++)
                            {
                                row.Add("'" + EEGdateString + "'");
                                EEGtimeRec = EEGtimeRec.AddMilliseconds(increment);
                                EEGdateString = EEGtimeRec.ToString(DateFormatForCSVFile);
                                EEGrefresh = false;
                                saveNextFilteredFrame = true;
                                
                                for (int j = 0; j < 16; j++)
                                {
                                    row.Add(Aura.records[i][j]);
                                }
                                
                                writer.WriteRow(row);
                                row.Clear();
                            }
                        }

                        yield return null;
                    }
                    this.StartCoroutineAsync(Matlab_File(), out task);
                    counter += 1;
                }
                lock (Aura.eventCounterKey)
                {
                    Aura.event1 = 0;
                }
            }
            yield return null;
        }
    }

    IEnumerator Matlab_File()
    {
        lock (Aura.busyWritingMatlabFile)
        {
            canAddMore = false;
        }
        MatlabData = new double[8][];
        for (int i = 0; i < 8; i++)
        {
            MatlabData[i] = Aura.matlabBuffers[i].ToArray();
        }
        MLDouble mlDoubleArray = new MLDouble("AURA_RAW_" + MATdateString, MatlabData);
        List<MLArray> mlList = new List<MLArray>();
        mlList.Add(mlDoubleArray);
        MatFileWriter mfw = new MatFileWriter(MatlabFilepath, mlList, false);

        lock (Aura.busyWritingMatlabFile)
        {
            doneWritingMatlabFile = true;
            canAddMore = true;
        }
        yield break;
    }

    IEnumerator FFT_File()
    {
        FFTpath = Path.Combine(MyDocsPath, "AURA_FFT___" + fileNameDateString + ".csv");
        using (CsvFileWriter writer = new CsvFileWriter(FFTpath))
        {
            CsvRow row = new CsvRow();
            row.Add("'FFT Bands'");
            for (int n = 0; n < 8; n++)
            {
                row.Add("Delta");
            }

            for (int n = 0; n < 8; n++)
            {
                row.Add("Theta");
            }

            for (int n = 0; n < 8; n++)
            {
                row.Add("Alpha");
            }

            for (int n = 0; n < 8; n++)
            {
                row.Add("Beta");
            }

            for (int n = 0; n < 8; n++)
            {
                row.Add("Gamma");
            }
            writer.WriteRow(row);
            row.Clear();
            row.Add("'Time and date'");
            for (int m = 0; m < 5; m++)
            {
                for (int n = 0; n < 8; n++)
                {
                    row.Add(names[n]);
                }
            }
            writer.WriteRow(row);
            row.Clear();
            row.Add("'[hh:mm:ss.mmm dd/mm/yyyy]'"); //* []
            for (int m = 0; m < 5; m++)
            {
                for (int n = 0; n < 8; n++)
                {
                    row.Add("Amplitude");
                }
            }
            writer.WriteRow(row);
            row.Clear();
            while (recording && Aura.streaming)
            {
                FFTtimeRec = DateTime.Now;
                FFTdateString = FFTtimeRec.ToString(DateFormatForCSVFile);
                row.Add("'" + FFTdateString + "'");
                foreach (string amplitude in intensities.deltasMax)
                {
                    row.Add(amplitude);
                }
                foreach (string amplitude in intensities.thetasMax)
                {
                    row.Add(amplitude);
                }
                foreach (string amplitude in intensities.alphasMax)
                {
                    row.Add(amplitude);
                }
                foreach (string amplitude in intensities.betasMax)
                {
                    row.Add(amplitude);
                }
                foreach (string amplitude in intensities.gammasMax)
                {
                    row.Add(amplitude);
                }
                writer.WriteRow(row);
                row.Clear();
                yield return null;
            }
        }
    }

    IEnumerator Filtered_File()
    {
        FilteredPath = Path.Combine(MyDocsPath, "AURA_Filtered___" + fileNameDateString + ".csv");
        using (CsvFileWriter writer = new CsvFileWriter(FilteredPath))
        {
            CsvRow row = new CsvRow();
            row.Add("'Time and date'");
            for (int n = 0; n < 8; n++)
            {
                row.Add(names[n]);
            }
            row.Add("'AccX'");
            row.Add("'AccY'");
            row.Add("'AccZ'");
            row.Add("'GyroX'");
            row.Add("'GyroY'");
            row.Add("'GyroZ'");
            row.Add("'Battery'");
            row.Add("'Event'");
            writer.WriteRow(row);
            row.Clear();
            row.Add("'[hh:mm:ss.mmm dd/mm/yyyy]'"); //* []
            for (int i = 0; i < 8; i++)
            {
                row.Add("'uV'"); //Add this 8 times
            }
            for (int i = 0; i < 3; i++)
            {
                row.Add("'G'"); //Add this 3 times
            }
            for (int i = 0; i < 3; i++)
            {
                row.Add("'Deg/s'"); //Add this 3 times
            }
            row.Add("'%'");
            row.Add("'Button'");
            writer.WriteRow(row);
            row.Clear();
            while (recording && Aura.running)
            {
                if (saveNextFilteredFrame)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        row.Add("'" + EEGdateString + "'");
                        EEGtimeRec = EEGtimeRec.AddMilliseconds(increment);
                        EEGdateString = EEGtimeRec.ToString(DateFormatForCSVFile);
                        saveNextFilteredFrame = false;

                        //Filtered
                        for (int j = 0; j < 8; j++)
                        {
                            row.Add(Aura.filteredSamples[j][Aura.filteredSamples[j].Length - 9 + i].ToString());
                        }
                        for (int j = 8; j < 16; j++)
                        {
                            row.Add(Aura.records[i][j]);
                        }

                        writer.WriteRow(row);
                        row.Clear();
                    }
                }

                yield return null;
            }
        }
    }


    private string GetMyDocumentsPath()
    {
        string persistentFile = Path.Combine(Application.persistentDataPath, "RecLocations.csv");
        if (File.Exists(persistentFile))
        {
            reader = new CsvFileReader(persistentFile);
            using (reader)
            {
                CsvRow row = new CsvRow();
                reader.ReadRow(row);
                string[] auxS = row.ToArray();
                if (auxS.Length != 0)
                {
                    Directory.CreateDirectory(auxS[0]);
                    return auxS[0];
                }
            }
        }

        string p = "";
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            p = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            p = Path.Combine(p, "Aura EEG Records");
            try { Directory.CreateDirectory(p); }
            catch (Exception)
            {
                Debug.Log("Error saving on MyDocuments, saving on internal folder:");
                p = Application.streamingAssetsPath;
                Debug.Log(p);
                Debug.Log("(Administrator Rights needed)");
            }
        }
        else if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            p = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            p = Path.Combine(p, "Aura EEG Records");
            try { Directory.CreateDirectory(p); }
            catch (Exception)
            {
                Debug.Log("Error saving on MyDocuments, saving on internal folder:");
                p = Application.streamingAssetsPath;
                Debug.Log(p);
                Debug.Log("( Administrator Rights needed )");
            }
        }
        else
        {
            p = Application.streamingAssetsPath;
        }
        return p;

    }
}
