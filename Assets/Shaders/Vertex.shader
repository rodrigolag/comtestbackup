﻿Shader "Custom/Vertex" {
	Subshader {
		BindChannels {
			Bind "vertex", vertex
			Bind "color", color
		}
		Pass {
			Stencil {
				Ref 1
				Comp Equal
			}
		}
	}
}